package fr.grunberg.gbsuftblai;

/**
 * Converts a number to a GbsuFtbLai string
 * @author Thomas Grunberg
 */
public class GbsuFtbLaiConverter {
	public static final String 
		 FACTOR3 = "Gbsu"
		,FACTOR5 = "Ftb"
		,FACTOR7 = "Lai"
		;
	
	
	/**
	 * Converts a number to a GbsuFtbLai string
	 * @param number the number to convert
	 * @return the converted number
	 */
	public static String convertToGbsuFtbLai(int number) {
		String result = "";
		String[] numbersAsStrings = String.valueOf(number).split("");
		
		if(number % 3 == 0)
			result = result + FACTOR3;
		if(number % 5 == 0)
			result = result + FACTOR5;
		
		for(String digit : numbersAsStrings) {
			if("3".equals(digit))
				result = result + FACTOR3;
			if("5".equals(digit))
				result = result + FACTOR5;
			if("7".equals(digit))
				result = result + FACTOR7;
		}
		
		if("".equals(result))
			return String.valueOf(number);
		return result;
	}
}
