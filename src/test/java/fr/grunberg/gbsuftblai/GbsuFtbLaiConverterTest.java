package fr.grunberg.gbsuftblai;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests JUnit 
 * @author Thomas Grunberg
 *
 */
public class GbsuFtbLaiConverterTest {
	@Test
	public void FactorNone() {
		assertEquals("1", GbsuFtbLaiConverter.convertToGbsuFtbLai(1));
	}

	@Test
	public void FactorGbsu() {
		assertEquals("GbsuGbsu", GbsuFtbLaiConverter.convertToGbsuFtbLai(3));
		assertEquals("Gbsu", GbsuFtbLaiConverter.convertToGbsuFtbLai(9));
		assertEquals("GbsuGbsuGbsu", GbsuFtbLaiConverter.convertToGbsuFtbLai(33));
	}

	@Test
	public void FactorFtb() {
		assertEquals("FtbFtb", GbsuFtbLaiConverter.convertToGbsuFtbLai(5));
	}

	@Test
	public void FactorLai() {
		assertEquals("Lai", GbsuFtbLaiConverter.convertToGbsuFtbLai(7));
	}

	@Test
	public void FactorCombined() {
		assertEquals("GbsuFtb", GbsuFtbLaiConverter.convertToGbsuFtbLai(51));
		assertEquals("FtbGbsu", GbsuFtbLaiConverter.convertToGbsuFtbLai(53));
		assertEquals("GbsuLai", GbsuFtbLaiConverter.convertToGbsuFtbLai(27));
		assertEquals("GbsuFtbFtb", GbsuFtbLaiConverter.convertToGbsuFtbLai(15));
	}
}
